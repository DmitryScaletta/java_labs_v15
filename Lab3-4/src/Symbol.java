public class Symbol {

	Symbol() {
		c = 0;
	}
	Symbol(Character newChar) { set(newChar); }

	private Character c;

	public Character get() { return c; }
	public void      set(Character newChar) { c = newChar; }

}