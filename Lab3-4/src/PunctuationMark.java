import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class PunctuationMark extends Symbol {

	private static final Set<Character> charsEOS = new HashSet<>(Arrays.asList(new Character[]{'.', '?', '!'})); // End Of Sentence
	private static final Set<Character> chars    = new HashSet<>(Arrays.asList(new Character[]{',', ':', ';', '-', '—', '"', '\'', '<', '>', '«', '»', '„', '“', '”', '‘', '’', '…', '[', ']', '(', ')', '{', '}'}));

	public static Boolean isPunctuationMark(Character c) {
		return charsEOS.contains(c) || chars.contains(c);
	}

	//PunctuationMark() {}
	PunctuationMark(Character c) throws PunctuationMarkException {
		super(c);
		if (charsEOS.contains(c)) {
			isEndOfSentence = true;
		} else
		if (chars.contains(c)) {
			isEndOfSentence = false;
		} else {
			throw new PunctuationMarkException("Это не знак приепинания.");
		}
	}

	private Boolean isEndOfSentence = false;
	public  Boolean isEndOfSentence() { return isEndOfSentence; }

}