import java.util.ArrayList;


public class Word {

	Word(ArrayList<Letter> newLetters) { set(newLetters); }

	public  ArrayList<Letter> letters = new ArrayList<>();
	private int lenght;

	public String get() {
		String s = new String();
		for (int i = 0; i < letters.size(); ++i) {
			s += letters.get(i).get();
		}
		return s;
	}
	public void set(ArrayList<Letter> newLetters) {
		letters = new ArrayList<>(newLetters);
		lenght  = letters.size();
	}

	public int lenght() { return lenght; }

	public Boolean equals(Word w) { return this.get().toLowerCase().equals(w.get().toLowerCase()); }

}