import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

/*
15.	Преобразовать каждое слово в тексте, удалив из него все последующие вхождения первой буквы этого слова.
*/

public class Main {

	private static void show(ArrayList<Sentence> sentences) {
		for (int i = 0; i < sentences.size(); ++i) {
			System.out.println(sentences.get(i).get());
		}
	}

	public static void main(String[] arg) {

		// 3

		String s;

		String filename = new String();
		// filename = "text.txt";

		try {
			s = new Scanner(new File(filename)).useDelimiter("\\Z").next();
		} catch (Exception e) {
			s = "В это время по дороге из города, по которой были расставлены махальные, показались два верховые. Это были адъютант и казак, ехавший сзади.";
		}

		s = s.replaceAll("\t"," ");
		s = s.replaceAll("\n"," ");
		while (s.indexOf("  ") != -1) { s = s.replaceAll("  ", ""); }

		ArrayList<Sentence> sentences = TextParser.parse(s);

		// оригинал
		show(sentences);

		// преобразование
		for (int i = 0; i < sentences.size(); ++i) {
			for (int j = 0; j < sentences.get(i).items().size(); ++j) {
				Object item = sentences.get(i).items().get(j);

				if (item instanceof Word) {
					Word word = (Word) item;
					Character first = word.letters.get(0).get();

					for (int k = 1; k < word.letters.size(); ++k) {
						Character curr = word.letters.get(k).get();
						if (curr.equals(first)) { word.letters.remove(k); }
					}
				}
			}

		}

		// результат
		show(sentences);



		// 4
		PunctuationMark myPunctuationMark;

		try {
			myPunctuationMark = new PunctuationMark('й');
			System.out.println("Результат: " + myPunctuationMark.get() + "\n");
		} catch (PunctuationMarkException e) {
			System.out.println("Ошибка: " + e.getMessage() + "\n");
		} catch (Exception e) {
			System.out.println("Ошибка..." + "\n");
		}
	}

}