public class PunctuationMarkException extends Exception{

	public PunctuationMarkException() {
		super("Не могу создать знак пунктуации.");
	}

	public PunctuationMarkException(String message) {
		super(message);
	}

}
