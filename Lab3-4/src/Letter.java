public class Letter extends Symbol {

	static Boolean isCapitalLetter(Character c) {
		return (c >= 65 && c <= 90)  || (c >= 1040 && c <= 1071) || (c == 1025);
	}
	static Boolean isSmallLetter(Character c) {
		return (c >= 97 && c <= 122) || (c >= 1072 && c <= 1103) || (c == 1105);
	}

	public static Boolean isLetter(Character c) { return isCapitalLetter(c) || isSmallLetter(c); }

	Letter(Character c) {
		super(c);
		if (isCapitalLetter(c)) {
			isCapitalLetter = true;
		} else {
			isCapitalLetter = false;
		}
	}

	private Boolean isCapitalLetter  = false;
	public  Boolean isCapitalLetter() { return isCapitalLetter; }

}