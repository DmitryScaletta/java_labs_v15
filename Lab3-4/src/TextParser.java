import java.util.ArrayList;


public class TextParser {

	static public ArrayList<Sentence> parse(String s) {

		ArrayList<Sentence> sentences = new ArrayList<>();

		ArrayList chars = new ArrayList(s.length());

		for (int i = 0; i < s.length(); ++i) {
			if (Letter.isLetter(s.charAt(i))) {
				chars.add(new Letter(s.charAt(i)));
			} else
			if (PunctuationMark.isPunctuationMark(s.charAt(i))) {
				try {
					chars.add(new PunctuationMark(s.charAt(i)));
				} catch (PunctuationMarkException e) {
					System.out.println("Error: " + e.getMessage());
				}
			} else {
				chars.add(new Symbol(s.charAt(i)));
			}
		}

		ArrayList newChars = new ArrayList();
		chars.stream().forEach(c -> {
			newChars.add(c);
			if (c instanceof PunctuationMark && ((PunctuationMark) c).isEndOfSentence()) {
				sentences.add(new Sentence(newChars));
				newChars.clear();
			}
		});

		if (newChars.size() > 0) sentences.add(new Sentence(newChars));

		return sentences;
	}

}