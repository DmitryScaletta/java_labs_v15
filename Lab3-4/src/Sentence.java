import java.util.ArrayList;


public class Sentence {

	private ArrayList items = new ArrayList();

	Sentence(ArrayList chars) {
		set(chars);
	}

	public ArrayList items() { return items; }

	public String get() {
		String s = new String();

		for (int i = 0; i < items.size(); ++i) {
			if (items.get(i) instanceof Symbol)          { s += ((Symbol)          items.get(i)).get(); } else
			if (items.get(i) instanceof Letter)          { s += ((Letter)          items.get(i)).get(); } else
			if (items.get(i) instanceof PunctuationMark) { s += ((PunctuationMark) items.get(i)).get(); } else
			if (items.get(i) instanceof Word)            { s += ((Word)            items.get(i)).get(); }
		}
		return s;
	}

	public void set(ArrayList chars) {

		// split into words
		ArrayList<Letter> letters = new ArrayList<>();

		for (int i = 0; i < chars.size(); ++i) {
			if (chars.get(i) instanceof Letter) {
				letters.add((Letter) chars.get(i));
			} else {
				// make word and add to array
				if (letters.size() > 0) {
					items.add(new Word(letters));
					letters.clear();
				}
				// add current item to array
				items.add(chars.get(i));
			}
		}

	}

}