public class NegativeLogException extends Exception{

    // конструктор по умолчанию
    public NegativeLogException() {
        super("Лагорифм от отрицательного числа.");
    }

    // конструктор с параметром, в который можно передать своё сообщение
    public NegativeLogException(String message) {
        super(message);
    }

}
