import java.util.Scanner;
import java.lang.Math;


class Main {

	public static void main(String[] arg) {

		// 1. Определить, на сколько квадрат разности двух заданных чисел больше их суммы.
		Scanner in = new Scanner(System.in);

		int a, b;

		System.out.println("Введите a, b");

		a = in.nextInt();
		b = in.nextInt();

		double sqs   = Math.pow(a - b, 2); // квадрат разности
		double sum   = a + b;              // сумма
		double delta = sqs - sum;

		System.out.println("Квадрат разности (" + sqs + ") двух заданных чисел (" + a + " и " + b + ") больше их суммы (" + sum + ") на: " + delta);


		// 2
		int     x, u;
		double  fx = 0;
		boolean div_zero = false;
		boolean negative = false;

		System.out.println("Введите x, u");

		x = in.nextInt();
		u = in.nextInt();

		if (x <= 0) {
			fx = Math.sqrt(u * x) * Math.pow(2, x + 1) * Math.cos(x);
		} else
		if (x < 4) {
			double div = Math.sqrt(Math.abs(x - 5 * u));
			if (div == 0) {
				div_zero = true;
			} else {
				fx = -x / div;
			}
		} else {
			// лагорфим от отрицательного числа не возможен
			// например, при x = 4, t будет отрицательным
			double t = Math.pow(x, 2) - 25;
			// проверяем, вляется ли t отрицательным
			try {
				// если t отрицательное, выбрасываем исключение
				if (t < 0) throw new NegativeLogException();
			} catch (NegativeLogException e) {
				// обрабатываем исключение
				// выводим сообщение об ошибке с помощью метода getMessage()
				System.out.println("Ошибка: " + e.getMessage());
				// выходим из программы
				return;
			}
			fx = Math.exp(x) * Math.log(t);
		}

		if (div_zero) {
			System.out.println("Деление на 0");
		} else {
			System.out.println("Результат: " + fx);
		}
	}
}