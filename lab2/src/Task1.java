import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;


public class Task1
{
	public static int readInt() {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int n = 0;
		while (true) {
			try {
				n = Integer.parseInt(br.readLine());
			} catch (Exception e) {
				System.out.println("Please, enter a corect number");
				continue;
			}

			if (n >= 2) break; else { System.out.println("Please, enter a corect number"); }
		}

		return n;
	}

	public static void doTask1() {

		// Дан массив A[n]. Подсчитать количество элементов массива с четными индексами, больших по абсолютной величине, чем k.

		System.out.println("Enter n");
		int n = readInt();

		System.out.println("Enter k");
		int k = readInt();


		int[] arr = new int[n];

		for (int i = 0; i < arr.length; ++i) {
			Random rand = new Random();
			int min = -99;
			int max = 99;
			arr[i] = rand.nextInt(max - min) + min;
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		int count = 0;
		for (int i = 0; i < arr.length; i += 2) {
			if (arr[i] > k) ++count;
		}

		System.out.println("Result: " + count + "\n\n");

	}
}
