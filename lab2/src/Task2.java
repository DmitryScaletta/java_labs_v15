import java.util.Random;
import java.util.Scanner;


public class Task2
{
	public static void doTask2() {

		// Дана матрица A[n][m]. В заданной строке каждый элемент, кроме первого, заменить суммой всех предыдущих. Отсортировать заданный столбец матрицы по убыванию.

		Scanner in = new Scanner(System.in);

		System.out.println("Enter n");
		int n = Task1.readInt();
		System.out.println("Enter m");
		int m = Task1.readInt();
		System.out.println("Enter row");
		int row = in.nextInt();
		System.out.println("Enter col");
		int col = in.nextInt();

		int[][]	a = new int[n][m];

		System.out.println("\nSource matrix");
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				Random rand = new Random();
				int min = -99;
				int max = 99;
				a[i][j] = rand.nextInt(max - min) + min;
				System.out.print(a[i][j] + "\t\t\t\t");
			}
			System.out.println();
		}

		System.out.println("\n");
		for (int i = 0; i < n; ++i) {
			if (i == row) {
				int sum = 0;
				for (int j = 0; j < m; ++j) {
					int t = a[i][j];
					if (j != 0) a[i][j] = sum;
					sum += t;
				}
			}
		}

		System.out.println("\n");
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				System.out.print(a[i][j] + "\t\t\t\t");
			}
			System.out.println();
		}

		System.out.println("\n");
		for (int j = 0; j < m; ++j) {
			if (j == col) {
				boolean swap = true;
				while (swap) {
					swap = false;
					for (int i = 0; i < n - 1; ++i) {
						if (a[i][j] < a[i + 1][j]) {
							int t = a[i + 1][j];
							a[i + 1][j] = a[i][j];
							a[i][j] = t;
							swap = true;
						}
					}
				}
			}
		}

		System.out.println("\nSorted matrix");
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				System.out.print(a[i][j] + "\t\t\t\t");
			}
			System.out.println();
		}
	}
}
